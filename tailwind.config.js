module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        dark: '#1f1e1e',
        light: '#fff1d8',
        primary: '#ffd000',
        secondary: '#5cfffa',
        tertiary: '#c783c9',
      },
    },
  },
  plugins: [],
}
