# Gooey.Dog Client

This project is a self-hosted, **React**-based browser client which interacts with a self-hosted Python backend. 

Gooey.Dog is a __portfolio website__ for the most part, showing off my work within various disciplines. You'll find things from little games, to art, to experiments with technologies.

## Want to view this site live?

Once live, the website is hosted at [Gooey.Dog](https://gooey.dog/)

## Information

- The artwork and content in this website is created by me (unless specified).
- This project is coded in TypeScript with an `ES5` target.
- This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
- Uses the following dependencies:
  - [react-router-dom](https://github.com/remix-run/react-router/tree/main/packages/react-router-dom) as a router
  - [Tailwind CSS](https://github.com/tailwindlabs/tailwindcss) configured with PostCSS
  - [ky](https://github.com/sindresorhus/ky) as a JavaScript library for requests
  - [SWR](https://github.com/vercel/swr) to use with `ky` for easier cached data fetching
  - [Typefaces](https://github.com/KyleAMathews/typefaces) to easily self-host Google Fonts

## Running this project

  The typical scripts laid out through the use of `create-react-app` are available.
  The following is taken directly from the README file created using `create-react-app`

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!
Will launch the app 

# Copyright and Licensing

## Code

The contents of this repository are licensed under the [Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/).

## Artwork

The artwork provided as templates is covered under the aforementioned license.

**Other artwork in this website signed under the name "Gui!", "Gui", "GuiHound", "구이!" or "구이", that is not included within this repository is not covered under the Mozilla Public License, as it is copyrighted under a Creative Commons License:**

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />The artwork in this website is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.

Any other artworks or resources are credited in a caption.
