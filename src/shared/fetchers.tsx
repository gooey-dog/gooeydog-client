import ky from "ky";
import { Fetcher, } from "swr";

export const getFetcher: Fetcher<any, string> = (key: string) => ky.get(`${process.env.REACT_APP_API}${key}`).json();
