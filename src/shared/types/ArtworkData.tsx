import { Url } from "url";

type ArtworkData = {
  id: number,
  title: string,
  description: string,
  URL: Url,
  Links: Array<Url>,
}

export default ArtworkData;