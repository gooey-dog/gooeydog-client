import { Url } from "url";

type QueueItemData = {
  id: number,
  owner?: string, // Anonymous if otherwise
  status: 'Status.QUEUED' | 'Status.CONFIRMED' | 'Status.STARTED' | 'Status.FINISHED',
  description: string,
  preview_url?: Url,
  updated: string, // date
}

export default QueueItemData;