import { Url } from 'url';

type CommissionStyleData = {
  id: number,
  name: string,
  previewUrl: Url,
}

type CommissionTypeData = {
  id: number,
  name: string,
}

type CommissionBackgroundData = {
  id: number,
  name: string,
  previewUrl: Url,
}

type CommissionExtrasData = {
  id: number,
  name: string,
  typeId: number,
  price: number
}

type CommissionPriceData = {
  typeId: number,
  styleId: number,
  price: number
}

type CommissionBackgroundPriceData = {
  typeId: number,
  backgroundId: number
  price: number
}

type CommissionAddonData = {
  id: number,
  name: string,
  priceMultiplier: number,
}

interface Commission {
  type?: CommissionTypeData;
  style?: CommissionStyleData;
  extras?: CommissionExtrasData[];
  background?: CommissionBackgroundData;
  addons?: CommissionAddonData[];
  price?: number;
}
  export type {Commission, CommissionStyleData, CommissionTypeData, CommissionBackgroundData, CommissionExtrasData, CommissionPriceData, CommissionBackgroundPriceData, CommissionAddonData}