import { Url } from "url";

type UserData = {
  discord_id: number,
  admin: boolean,
  username: string,
  discriminator: string,
  avatar: Url,
}

export default UserData;