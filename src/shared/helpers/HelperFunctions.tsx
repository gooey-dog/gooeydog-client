export const capitalize = (s: string): string => {
  return s.charAt(0).toUpperCase() + s.slice(1);
}

export const addOrUndefined = (a: number | undefined, b: number | undefined) => {
  return (a !== undefined && b !== undefined && a + b) || undefined;
}