import React from "react";
import UserData from "../types/UserData";

const UserContext = React.createContext<{
    user: UserData | undefined, isLoading: boolean, error: any, login: Function | undefined, logout: Function | undefined, callback: Function | undefined
  }>({
    user: undefined, isLoading: true, error: null, login: undefined, logout: undefined, callback: undefined
  });

export default UserContext;