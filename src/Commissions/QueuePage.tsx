import { FC, useRef } from 'react'
import Background from '../Components/Background';
import CommissionQueue from '../Components/CommissionQueue';
import Footer from '../Components/Footer';
import Header from '../Components/Header';

const QueuePage:FC = () => {
  const containerRef = useRef<HTMLElement>(null);
  return (
    <main ref={containerRef} className="flex flex-col w-full min-h-screen max-h-screen overflow-y-auto items-center">
      <Background />
      <Header containerRef={containerRef} threshold={0} fixed={true} />
      <div className="my-20"/>
      <div className="flex flex-col items-center md:items-start md:flex-row h-full w-full max-w-screen-md grow">
        <div className="flex flex-row justify-center items-center md:flex-col w-full md:w-auto grow bg-white/50 h-fit p-3 rounded-xl sm:mr-2 max-w-screen-sm" >
          <h1 className="text-dark text-2xl md:mb-4 mr-4">Legend: </h1>
          <div className="content-center">
            <span className="w-6 h-6 bg-gray-300 rounded-full inline-block" />
            <span className="py-auto text-center align-top mr-2">Queued</span>
          </div>
          <div className="content-center">
            <span className="bg-secondary w-6 h-6 rounded-full inline-block" />
            <span className="py-auto text-center align-top mr-2">Confirmed</span>
          </div>
          <div className="content-center">
            <span className="bg-tertiary w-6 h-6 rounded-full inline-block" />
            <span className="py-auto text-center align-top mr-2">Started</span>
          </div>
          <div className="content-center">
            <span className="bg-primary w-6 h-6 rounded-full inline-block" />
            <span className="py-auto text-center align-top mr-2">Finished</span>
          </div>
        </div>
        <CommissionQueue />
      </div>
      <Footer />
    </main>
  )
}

export default QueuePage