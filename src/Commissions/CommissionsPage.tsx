import {FC, useRef} from 'react'
import Background from '../Components/Background';
import Footer from '../Components/Footer';
import Header from '../Components/Header';
import IconInfoBox from '../Components/IconInfoBox';
import withLink from '../Components/withLink';

const LinkBox = withLink(IconInfoBox);

const CommissionsPage:FC = () => {
  const containerRef = useRef<HTMLElement>(null);

  return (
    <main className="w-full min-h-screen flex flex-col items-center">
      <Background />
      <Header containerRef={containerRef} threshold={0} fixed={false}/>
      <h1 className="text-4xl sm:text-5xl lg:text6xl font-semibold">
        Commissions
      </h1>
      <div className="flex flex-row flex-wrap w-full h-full justify-center items-center grow">
        <LinkBox 
          linkTo="/commissions/quote"
          title="Get a Quote!"
          body={["Want some art for yourself?",
                 "Want to know my rates?",
                 "Use this handy tool!"
               ]}
        />
        <LinkBox 
          linkTo="/commissions/queue"
          title="See the Queue!"
          body={["Want to check the Queue?",
                 "Want updates on your commission?",
                 "Check out the things I'm working on!"
               ]}
        />
      </div>
      <Footer />
    </main>
  )
}

export default CommissionsPage