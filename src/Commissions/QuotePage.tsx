import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getFetcher } from "../shared/fetchers";
import useSWRLoading from "../hooks/useSWRLoading";
import * as comm from "../shared/types/CommissionInfoData";
import Background from "../Components/Background";
import CommissionPreview from '../Components/CommissionPreview';
import OptionSelector from '../Components/OptionSelector';
import ReceiptModal from '../Components/ReceiptModal';
import { addOrUndefined, capitalize } from "../shared/helpers/HelperFunctions"

const DefaultCommission: comm.Commission = {
  type: undefined,
  style: undefined,
  extras: [],
  background: undefined,
  addons: [],
  price: undefined,
};

const QuotePage = (): JSX.Element => {
  const {data, isLoading, error} = useSWRLoading<{
    types: comm.CommissionTypeData[], 
    styles: comm.CommissionStyleData[], 
    extras: comm.CommissionExtrasData[], 
    backgrounds: comm.CommissionBackgroundData[], 
    prices: comm.CommissionPriceData[], 
    backgroundPrices: comm.CommissionBackgroundPriceData[], 
    addons: comm.CommissionAddonData[] 
  }>("/commissions/info", getFetcher);

  const [commission, setCommission] = useState<comm.Commission>(DefaultCommission);
  const [waiting, setWaiting] = useState<boolean>(false);
  const [showModal, setShowModal] = useState<boolean>(false);

  // Calculates price
  useEffect(() => {
    const calculatePrice = (commission: comm.Commission) : number | undefined => {

      if(data) {
        if(commission.type && commission.style) {
          let price: number | undefined = 0;
          price = data.prices.find((e) => e.typeId === commission.type?.id && e.styleId === commission.style?.id)?.price;
          if(commission.background) {
            price = addOrUndefined(price, data.backgroundPrices.find((e) => e.typeId === commission.type?.id && e.backgroundId === commission.background?.id)?.price);
          }
          commission.extras?.forEach((extra) => {
            price = addOrUndefined(price, extra.price);
          });
          commission.addons?.forEach((addon) => {
            price = price && price * addon.priceMultiplier;
          });
          return price;
        } 
      } else {
        return undefined;
      }
    }

    setCommission({...commission, price: calculatePrice(commission)})
  }, [commission.type, commission.style, commission.background, commission.extras, commission.addons])

  // Displays a drawing screen
  useEffect(() => {
    setWaiting(true);
    setTimeout(() => {
      setWaiting(false);
    }, 1500);

  }, [commission.type, commission.style, commission.background])

  const handleClear = () => {
    setCommission(DefaultCommission);
  }
  const handleTypeChange = (typeName: string) => {
    const newType = data?.types.find((t) => t.name === typeName)
    setCommission({...commission, type: newType})
  }
  const handleStyleChange = (styleName: string) => {
    const newStyle = data?.styles.find((s) => s.name === styleName)
    setCommission({...commission, style: newStyle})
  }
  const handleBackgroundChange = (backgroundName: string) => {
    const newBackground = data?.backgrounds.find((b) => b.name === backgroundName)
    setCommission({...commission, background: newBackground})
  }
  const handleAddonsChange = (addonName: string) => {
    const newAddon = data?.addons.find((a) => a.name === addonName);
    let newAddons: comm.CommissionAddonData[] | undefined = [];
    if(newAddon){
      if(commission.addons?.includes(newAddon)){
        newAddons = commission.addons.filter((a) => a !== newAddon);
      } else {
        newAddons = commission.addons?.concat(newAddon);
      }
      setCommission({...commission, addons: newAddons})
    }
  }

  return (
    <main className="w-full min-h-screen lg:h-screen">
      <Background />
      <div className="w-full h-full lg:h-screen sm:p-4 lg:p-8 py-4 relative">
        <div className="flex flex-col lg:flex-row-reverse h-full bg-white rounded-2xl text-dark shadow-lg shadow-white/50">
          { isLoading && 
            <div className="text-3xl text-tertiary drop-shadow-md">
              Loading commission info... 
            </div> 
          }
          { error && 
            <div className="text-2xl text-red-500 drop-shadow-md">
              There's been an error. Contact me directly to get a quote, or try again later!
            </div>
          }
          { data && 
            <form onSubmit={(e) => e.preventDefault()} className="flex flex-col items-center justify-start h-full text-2xl text-dark font-light p-8">
              <h1 className="text-5xl p-2 pt-8">
                Get a Quote!
              </h1>
              <button
                name="reset"
                type="button"
                onClick={handleClear}
                className="text-2xl text-dark font-light p-1 px-4 bg-neutral-300 rounded-lg mb-6"
              >
                Reset
              </button>
              <OptionSelector 
                label={"Commission Type:"}
                value={commission.type?.name ?? ''}
                handler={(e:any) => handleTypeChange(e.target.value)}
                options={data.types}
              />
              { commission.type &&
                <OptionSelector 
                  label={"Art Style:"}
                  value={commission.style?.name ?? ''}
                  handler={(e:any) => handleStyleChange(e.target.value)}
                  options={data.styles.filter((style) => data.prices.find((priceEntry) => style.id === priceEntry.styleId && commission.type?.id === priceEntry.typeId))}
                />
              }
              { commission.style && 
                <OptionSelector 
                  label={"Background:"}
                  value={commission.background?.name ?? ''}
                  handler={(e:any) => handleBackgroundChange(e.target.value)}
                  options={data.backgrounds}
                />
              }
              {commission.background && 
                <>
                  <div className="text-xl mt-6">
                    Add-ons:
                  </div>
                  {data.addons.map((a) => 
                    <label key={a.id}>
                      {capitalize(a.name)}
                      <input 
                        value={a.name}
                        type="checkbox"
                        checked={commission.addons?.includes(a)}
                        onChange={(e) => handleAddonsChange(e.target.value)}
                      />
                    </label>)
                  }
                  <button onClick={() => setShowModal(true)} className="hover:scale-105 transition-all bg-neutral-200 p-2 px-4 my-6 rounded-lg hover:shadow-lg hover:bg-gradient-to-br hover:from-tertiary hover:to-purple-600">
                    <div>
                      Looks Good!
                    </div>
                  </button>
                </>
              }
              <div className="fixed z-10 text-xl text-neutral-500 bottom-6 right-6 w-52 p-2 bg-neutral-300/60 rounded-lg shadow-lg">
                <div>Your Quote:</div>
                <div className="text-4xl text-dark">
                  USD {commission.price?.toFixed(2)}
                </div>
              </div>
            </form>
          }
          <CommissionPreview commission={commission} />
        </div>
        <Link to="/" className="absolute bg-primary h-24 w-24 rounded-full top-1 left-1 hover:scale-105 transition-all text-center flex flex-col justify-center text-2xl shadow-lg">
          <img src={`${process.env.REACT_APP_IMG_SRC}/Logo-Minimal.svg`} className="p-6 -hue-rotate-30" />
        </Link>
      </div>
      { showModal && 
        <ReceiptModal 
          commission={commission}
          onClick={() => setShowModal(false)}
        />
      }

    </main>
  )
}

export default QuotePage