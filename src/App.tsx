import React, { FC, Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';
import AccountPage from './AccountPage';
import HomePage from './HomePage';
import useSWRUser from './hooks/useSWRUser';
import LoadingPage from './LoadingPage';
import NotFoundPage from './NotFoundPage';
import UserContext from './shared/contexts/UserContext';
import { getFetcher } from './shared/fetchers';
import UserData from './shared/types/UserData';
import UserCallbackPage from './UserCallbackPage';

const CommissionsPage = React.lazy(() => import('./Commissions/CommissionsPage'))
const QuotePage = React.lazy(() => import('./Commissions/QuotePage'))
const QueuePage = React.lazy(() => import('./Commissions/QueuePage'))
const ArtPage = React.lazy(() => import('./ArtPage'))

const App:FC = () => {
  const { user, isLoading, error, login, logout, callback} = useSWRUser();

  return (
    <div className=" text-light">
      <UserContext.Provider value={{user, isLoading, error, login, logout, callback}}>
        <Suspense fallback={<LoadingPage/>}>
          <Routes>
            <Route path="/" element={ <HomePage /> } />
            <Route path="/art" element={ <ArtPage /> } />
            <Route path="/commissions/" element={ <CommissionsPage /> } />
            <Route path="/commissions/quote" element={ <QuotePage/> } />
            <Route path="/commissions/queue" element={ <QueuePage /> } />
            <Route path="*" element={ <NotFoundPage /> } />
            <Route path="/account" element={ <AccountPage /> } />
            {/* This stuff here isn't publicly linked to. */}
            <Route path="/user/callback" element={ <UserCallbackPage /> } />
            <Route path="/dev/loading" element={ <LoadingPage /> } />
          </Routes>
        </Suspense>
      </UserContext.Provider>
    </div>
  );
};

export default App;
