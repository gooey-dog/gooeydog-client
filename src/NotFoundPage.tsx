import { FC, useRef } from "react";
import { Link } from "react-router-dom";
import Header from "./Components/Header";

const NotFoundPage:FC = () => {
  const containerRef = useRef<HTMLElement>(null);

  return (
    <main ref={containerRef} className="bg-dark h-screen flex flex-col">
      <Header containerRef={containerRef} threshold={0} fixed={false}/>
      <div className="flex flex-col justify-center items-center grow">
        <h1 className="text-4xl text-light p-4">Page not found in the application.</h1>
        <Link to="/" className="btn-primary text-2xl">Home</Link>
      </div>
    </main>
  );
};

export default NotFoundPage;