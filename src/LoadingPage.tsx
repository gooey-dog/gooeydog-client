import { FC } from "react"
import Background from "./Components/Background";

const LoadingPage:FC = () => {
  return (
    <main className="flex flex-col justify-center w-full h-screen text-center text-6xl text-tertiary items-center">
      <Background />
      <img src={`${process.env.REACT_APP_IMG_SRC}/Logo-white.svg`} className="text-center h-60 drop-shadow-[0_0.5rem_0.5rem_rgba(255,255,255,0.15)]" />
      <h1 className="bg-gradient-to-br from-tertiary to-secondary bg-clip-text text-transparent p-4 h-fit w-fit animate-pulse">Loading...</h1>
    </main>
  )
}

export default LoadingPage