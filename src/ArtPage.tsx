import {FC, useRef} from 'react'
import Background from './Components/Background';
import Header from './Components/Header';
import CarouselHero from './Components/CarouselHero';
import Footer from './Components/Footer';

const ArtPage:FC = () => {
  const containerRef = useRef<HTMLElement>(null);

  return (
    <main className="flex flex-col w-full min-h-screen">
      <Background />
      <Header containerRef={containerRef} threshold={0} fixed={true} />
      <CarouselHero />
      <div className="grow">
        This will contain data.

      </div>
      <Footer />
    </main>
  )
}

export default ArtPage