import { userInfo } from "os";
import { useContext } from "react";
import Background from "./Components/Background";
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import UserContext from "./shared/contexts/UserContext";

const AccountPage= (): JSX.Element => {
  const {user, isLoading, login, logout} = useContext(UserContext);

  return (
    <main className="flex flex-col w-full min-h-screen">
      <Background/>
      <Header 
        threshold={0}
        fixed={false}
      />
      <div className="grow">
        
        <button onClick={() => logout && logout()}>
          Logout
        </button>
      </div>
      <Footer />
    </main>
  )
}

export default AccountPage;