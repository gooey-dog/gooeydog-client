import useSWR, { BareFetcher, Key } from "swr";

/*
  Taken from:
  https://swr.vercel.app/docs/getting-started
*/
export default function useSWRLoading<T>(key:Key, fetcher:BareFetcher<T>){
  const { data, error } = useSWR<T>(key, fetcher)

  return {
    data: data,
    isLoading: !error && !data,
    error: error
  }
}