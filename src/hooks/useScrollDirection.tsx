import { useState, useEffect, RefObject } from "react";
import useScroll from "./useScroll";

export default function useScrollDirection(containerRef?: RefObject<HTMLElement>) {
  const [prevScroll, setPrevScroll] = useState<number>(0);
  const [direction, setDirection] = useState<boolean>(false);
  const scroll = useScroll(containerRef);

  useEffect(() => {
    if(prevScroll >= scroll) {
      setDirection(false);
    } else {
      setDirection(true);
    }
    setPrevScroll(scroll);
  }, [scroll])
  return direction;
}