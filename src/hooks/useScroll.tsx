import { Ref, RefObject, useEffect, useState } from 'react';

export default function useScroll(containerRef?: RefObject<HTMLElement>) {
  const [scroll, setScroll] = useState<number>(0);

  const onScroll = () => {
    if(containerRef && containerRef.current) {
      setScroll(containerRef.current.scrollTop);
    }
  }

  useEffect(() => {
    if(containerRef && containerRef.current)
    containerRef.current.addEventListener('scroll', onScroll);

    return () => {containerRef && containerRef.current && containerRef.current.removeEventListener('scroll', onScroll);}
  }, []);

  return scroll;
}