import { RefObject, useEffect, useState } from 'react';
import useScroll from './useScroll';

export default function useScrollVisibility(threshold: number, containerRef: RefObject<HTMLElement>) {
  const [visible, setVisible] = useState<boolean>(threshold === 0 ? true:false);
  const scroll = useScroll(containerRef);

  useEffect(() => {
    if(scroll >= threshold) {
      setVisible(true)
    } else {
      setVisible(false)
    }

  }, [scroll]);

  return visible;
}