import { useState, useEffect } from 'react';

export default function useFirstTimeVisitor() {
  const [visited, setVisited] = useState<boolean>(true);

  useEffect( () => {
    const visited = localStorage.getItem('visited');
    setVisited(visited ? true : false);
    localStorage.setItem('visited', 'true');
  }, []);

  return visited;
};  