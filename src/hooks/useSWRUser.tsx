import ky from "ky";
import { useEffect, useState } from "react";
import UserData from "../shared/types/UserData";

export default function useSWRUser() {
  const [user, setUser] = useState<UserData>();
  const [error, setError] = useState<any>();

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    try {
      const response = await ky.get(`${process.env.REACT_APP_API}/user/`, {
        credentials: 'include'
      }).json();
      //console.log(response as UserData);
      setUser(response as UserData);
    } catch(e: any) {
      if(e.response.status == 401){
        setUser(undefined);
      }
      setError(e);
    }
  }

  const login = async () => {
    try {
      const response = await ky.get(`${process.env.REACT_APP_API}/user/login`, {
        redirect: 'manual',
        throwHttpErrors: true
      }).json() as {state: string, redirect_url: string};
      //console.log(response)
      localStorage.setItem('state', response.state);
      window.location.replace(response.redirect_url);
    } catch(e: any) {
      console.log(e.response);
      setError(e);
    }
  }

  const callback = async (url: string) => {
    try {
      const response = await ky.post(`${process.env.REACT_APP_API}/user/callback`, {
        credentials: 'include',
        json: {
          'url': url,
          'state': localStorage.getItem('state')
        }
      }).json();
      //console.log(response as UserData);
      setUser(response as UserData);
    } catch (e: any){
      console.log(e);
      setError(e);
    }
  }

  const logout = async () => {
    try {
      const response = await ky.get(`${process.env.REACT_APP_API}/user/logout`, {
        credentials: 'include'
      }).json();
      console.log(response);
      setUser(undefined);
      setError({response: { status : 401}})
    } catch(e: any) {
      console.log(e);
      setError(e);
    }
  }

  return {
    user: user,
    isLoading: !error && !user,
    error: error?.response?.status == 401 ? undefined : error,
    login,
    logout,
    callback
  }
}