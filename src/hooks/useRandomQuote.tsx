import { useState, useEffect } from "react";

const useRandomQuote = () => {
  const [randomQuote, setRandomQuote] = useState<string>("");

  useEffect(() => {
    const quoteBank = [
      "Something random, something funny.",
      "There when you need it.",
      "For your artistic and technical needs.",
      "Like a tamagotchi.",
      "Won't let you down",
      "Gotta have one.",
    ];
    const randomFromArray = <T extends unknown>(array:Array<T>) => {
      return array[Math.floor(Math.random() * array.length)];
    };

    setRandomQuote(randomFromArray(quoteBank));
  }, []);

  return randomQuote;
};

export default useRandomQuote;