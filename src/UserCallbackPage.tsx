import { FC, useContext, useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import Background from "./Components/Background";
import Footer from "./Components/Footer";
import UserContext from "./shared/contexts/UserContext";

const UserCallbackPage:FC = () => {
  const [success, setSuccess] = useState<boolean>(false);

  const { callback } = useContext(UserContext);

  useEffect(() => {
    callback && callback(window.location.href);
    setSuccess(true);
  }, [])

  return (
    <main className="w-full min-h-screen flex flex-col items-center">
      <Background/>
        <div className="flex flex-col justify-center grow">
          <h1 className="py-4 text-6xl text-center font-bold bg-gradient-to-br from-primary to-secondary text-transparent bg-clip-text animate-pulse">
            Logging you in with Discord!
          </h1>
          { success && <Navigate replace to="/"/> }
        </div>
      <Footer/>
    </main>
  )
}

export default UserCallbackPage;