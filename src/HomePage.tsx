import { FC, useRef } from 'react';
import IconInfoBox from './Components/IconInfoBox';
import Background from './Components/Background';
import Hero from './Components/Hero';
import useFirstTimeVisitor from './hooks/useFirstTimeVisitor';
import WelcomeModal from './Components/WelcomeModal';
import withLink from './Components/withLink';
import Header from './Components/Header';

const HomePage:FC = () => {
  const visited = useFirstTimeVisitor();
  const mainRef = useRef<HTMLElement>(null);

  const LinkedIconInfo = withLink(IconInfoBox);
  return (
    <main ref={mainRef} className="flex flex-col items-center justify-start overflow-y-scroll  h-screen max-h-screen max-w-screen w-full snap-y snap-proximity">
      <Header threshold={0} fixed={true} containerRef={mainRef}/>
      { !visited && <WelcomeModal/> }
      <Background />
      <Hero /> 
      <div className="bg-white w-full h-20 shadow-xl shadow-white/50" />
      <div className="w-full py-16 sm:py-24 max-w-screen-xl snap-start">
        <div className="flex flex-row justify-evenly flex-wrap">
          <LinkedIconInfo 
            title="Art"
            body={["My gallery!"]}
            linkTo="/art"
          />
          <LinkedIconInfo
            title="Commissions"
            body={["Want some art?"]}
            linkTo="/commissions"
          />
          <LinkedIconInfo
            title="Other"
            body={["Other cool stuff!"]}
            linkTo="/other"
          />
          <LinkedIconInfo
            title="Login..."
            body={["Login with Discord!"]}
            linkTo="/login"
          />
        </div>
      </div>
      <div className="bg-dark shadow-xl shadow-white/50 w-full snap-start">
        <div className="flex flex-col sm:flex-row justify-center max-w-screen-xl mx-auto">
          <div className="p-16 px-4 md:px-16 sm:w-3/4">
            <h1 className="text-3xl sm:text-4xl lg:text-5xl py-4 font-semibold text-primary">What is Gooey.Dog?</h1>
            <div className="text-2xl sm:text-2xl lg:text-3xl px-4 font-light">
              <p className="py-2">Great question!</p>
              <p className="py-2">This is my personal website, where I showcase my art, let you track your commissions, give you quotes on a potential commission you're thinking of getting, and showcase little projects and games! </p>
              <p className="py-2">You can log in by using your Discord account (be sure to check the URL you're redirected to). This'll let you track your progress in this site's minigames, access info regarding your commissions, and communicate with me easily. </p>
            </div>
          </div>
          <div className="grow">
            <img src={`${process.env.REACT_APP_IMG_SRC}/ProfWuff.png`} className="w-full h-full object-cover bg-black" />
          </div>
        </div>
      </div>
      <div className="bg-white shadow-xl shadow-white/50 max-w-screen-xl my-0 snap-start">
        <IconInfoBox 
          title="What's this?"
          body={[
            "Great question!",
            "This is my personal website, where I showcase my art, let you track your commissions, give you quotes on a potential commission you're thinking of getting, and showcase little projects and games!",
            "You can log in through the OAuth2 (Explicit Grant) protocol by using your Discord account. This'll let you track your progress in this site's minigames, access info regarding your commissions, and communicate with me easily.",
          ]}
        />
        <IconInfoBox 
          title="What's with the Dog?"
          body={[
            "That's your very own Gooey Dog!",
            "Check in every once in a while and you'll see an option under your Gooey Dog to feed them a variety of things.",
            "Gain points by feeding them Bones!",
            "Collect items that may sometimes appear, and equip them in the customize page!",
            "Check out the Points Shop to buy a variety of items!",
            "Check out the leaderboard to see how well you're caring for your Gooey Dog in comparison to others!",
          ]}
        />
      </div>
    </main>
  );
};

export default HomePage;