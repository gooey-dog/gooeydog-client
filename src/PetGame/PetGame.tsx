import React from 'react'

const PetGame = () => {
  return (
    <div className="bg-white z-5 w-11/12 lg:w-auto lg:h-full aspect-[3/2] mx-auto drop-shadow-2xl shadow-white/40 rounded mb-16 bg-opacity-75" />
  )
}

export default PetGame;