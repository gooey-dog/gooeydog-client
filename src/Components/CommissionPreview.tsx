import * as comm from "../shared/types/CommissionInfoData";

const CommissionPreview = ({ commission }: { commission: comm.Commission; }): JSX.Element => {
  return (
    <div className="bg-neutral-800 w-full h-[40rem] lg:h-full rounded-b-2xl lg:rounded-l-2xl lg:rounded-r-none relative">
      <div className="absolute top-4 right-4 bg-white p-4 rounded-lg font-light text-xl shadow-md shadow-white/30">
        Preview
      </div>
      {commission.style &&
        <img src={`${process.env.REACT_IMG_SRC}/quotePreviews/${commission.style.previewUrl}`} />}

    </div>
  );
};

export default CommissionPreview;