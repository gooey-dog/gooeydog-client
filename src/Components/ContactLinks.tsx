const ContactLinks = () : JSX.Element => {
  return (
    <div className="flex flex-row justify-center w-full max-w-screen-sm text-2xl">
      <a href="https://twitter.com/GuiHound" className="h-14 w-14 m-4">
        <img src={`${process.env.REACT_APP_IMG_SRC}/Twitter-logo.svg`} className="h-auto w-auto"/>
      </a>
      <a href="mailto:gongpoarts@gmail.com" className="h-14 w-14 m-4">
        <img src={`${process.env.REACT_APP_IMG_SRC}/Email-icon.svg`} className="h-full w-full"/>
      </a>
    </div>
  )
}

export default ContactLinks;