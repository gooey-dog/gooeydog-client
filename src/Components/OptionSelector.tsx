import { capitalize } from "../shared/helpers/HelperFunctions";
import * as comm from "../shared/types/CommissionInfoData"

const OptionSelector = ({
  label,
  value,
  handler,
  options
}: {
  label: string;
  value: string;
  handler: any;
  options: comm.CommissionTypeData[] | comm.CommissionStyleData[] | comm.CommissionBackgroundData[];
}): JSX.Element => {
  const optionClasses = "text-dark";
  return <>
      <label>{label}</label>
      <select value={value} onChange={handler} className="p-2 w-72 rounded font-light hover:scale-x-105 hover:border-tertiary hover:border-2 transition-all active:border-secondary active:border-2 leading-tight bg-neutral-100">
        <option className={optionClasses} key={0} value=''></option>
        {options?.map((option: any) => <option className={optionClasses} key={option.id} value={option.name}>{capitalize(option.name)}</option>)}
      </select>
    </>;
};

export default OptionSelector;
  