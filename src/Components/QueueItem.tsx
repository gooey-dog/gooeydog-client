import { FC, useState } from "react"
import QueueItemData from "../shared/types/QueueItemData"

const QueueItem:FC<QueueItemData> = ( data: QueueItemData ) => {
  const [expanded, setExpanded] = useState<boolean>(false);

  const handleClick = () => {
    setExpanded(!expanded);
  }

  const statusToColour = (status: string): string => {
    switch (status){
      case "Status.QUEUED":
        return "bg-gray-300";
      case "Status.CONFIRMED":
        return "bg-secondary";
      case "Status.STARTED":
        return "bg-tertiary";
      case "Status.FINISHED":
        return "bg-primary";
      default:
        return "";
    }
  }

  return (
    <div className="bg-dark w-full h-auto p-6 m-1 mx-4 rounded-lg shadow-md shadow-black/40 hover:scale-[102%] hover:cursor-pointer transition-all duration-300" onClick={handleClick}>
      <div className="flex flex-row text-light text-xl font-light">
        <div className="grow">
          <h1 className="text-3xl text-primary pb-2">
            {data.owner ?? "Anonymous"}
          </h1>
          <p>{data.description}</p>
        </div>
        <div className={`${statusToColour(data.status)} w-16 h-16 rounded-full my-auto`} />
      </div>
      { expanded && 
        <div className="p-4 flex flex-row">
          <div className="grow text-xl font-light">
            <div>
              Status: {data.status}
            </div>
            <div className="text-lg">
              Last Updated: {data.updated}
            </div>
          </div>
          {data.preview_url &&
            <img src={`${data.preview_url}`} className="max-w-1/2 max-h-[20rem] object-contain"/>
          }
        </div>
      }
    </div>
  )
}

export default QueueItem