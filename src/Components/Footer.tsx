import { FC } from 'react'
import bg from '../assets/FooterBG.svg'
import ContactLinks from './ContactLinks';

const Footer = () => {
  return (
    <footer className="flex flex-col items-center justify-center w-full bg-cover bg-top bg-no-repeat h-[17rem] pt-20 " style={{backgroundImage: `url(${bg})`}}>
      <div className="text-neutral-600 font-extralight text-2xl p-2">
        Contact Me!
      </div>
      <ContactLinks />
      <div className="text-neutral-600 font-extralight">
        Copyright © 2022 Gongpoarts.
      </div>
    </footer>
  )
}

export default Footer