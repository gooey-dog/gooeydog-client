import bgstars from '../assets/StarsBG.svg';

const Background = () => {
  return (
    <>
      <div className="fixed bg-fixed bg-gradient-to-bl from-black to-neutral-800 w-full h-screen -z-10" />
      <div className="fixed bg-fixed w-full h-screen -z-10 bg-repeat" style={{backgroundImage: `url(${bgstars})`}}/>
    </>
  )
}

export default Background