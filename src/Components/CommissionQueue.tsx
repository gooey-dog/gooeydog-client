import { FC, } from 'react';
import QueueItem from './QueueItem';
import QueueItemData from '../shared/types/QueueItemData';
import { getFetcher } from '../shared/fetchers';
import { Link } from 'react-router-dom';
import useSWRLoading from '../hooks/useSWRLoading';

const CommissionQueue:FC = () => {
  const { data, isLoading, error } = useSWRLoading<Array<QueueItemData>>("/commissions/queue", getFetcher)

  const finishedFilter = (item: QueueItemData) => item.status !== "Status.FINISHED"


  return (
    <div className="flex flex-col rounded-xl items-center max-w-screen-sm w-full grow bg-white sm:p-6 mb-6 shadow-xl shadow-white/40">
      { data && <>
        <h1 className="text-dark bg-clip-text text-5xl p-4 mb-2 font-semibold">
          Active Queue
        </h1>
        <div className="text-md text-gray-500 mb-2">
          Click to expand...
        </div>
        { 
          (data.filter(finishedFilter).length !== 0 ? 
            (data.filter(finishedFilter).map((queueItem) => <QueueItem key={queueItem.id} {...queueItem} />))
            : <div className="flex flex-col items-center bg-dark text-light text-2xl p-6 rounded-xl shadow-md shadow-black/50 text-center">
              <div>
                The Queue is empty, now's your chance!
              </div>
              <Link to="/commissions/quote" className="p-4 w-fit rounded-lg mt-4 bg-gradient-to-bl from-tertiary to-purple-600 text-2xl text-light hover:scale-105 hover:from-primary hover:to-tertiary transition-all duration-200">
                Get your own!
              </Link>
            </div>
          )
        }
        <h1 className="text-dark bg-clip-text text-5xl p-4 mb-2 font-semibold">
          Finished Commissions 
        </h1>
        {
          data.filter((item) => !finishedFilter(item)).reverse().map((queueItem) => <QueueItem key={queueItem.id} {...queueItem} />)
        }
      </>}
      { isLoading &&
        <div className="text-4xl bg-gradient-to-br from-tertiary to-secondary bg-clip-text text-transparent p-4 h-fit w-fit animate-pulse">
          Loading...
        </div>
      }
      { error && 
        <div className="bg-gradient-to-br from-red-500 to-red-800 bg-clip-text text-transparent p-4 h-fit w-fit text-4xl">
          <h1>
            Error Retrieving Posts! 
          </h1>
          <p className="text-xl">
            Please try again later
          </p>
        </div>
      }
    </div>
  )
}

export default CommissionQueue