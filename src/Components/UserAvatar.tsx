import {FC, useContext} from 'react'
import { Link } from 'react-router-dom';
import UserContext from '../shared/contexts/UserContext';
import admincrown from '../assets/AdminCrown.svg'
import spinner from '../assets/spinner.svg'

const UserAvatar:FC = () => {
  const {user, isLoading, error, login} = useContext(UserContext);

  const avatarUrl = `https://cdn.discordapp.com/avatars/${user?.discord_id}/${user?.avatar}.png`

  return (
    <div className="py-6 px-2 sm:mx-3 w-20 group hover:scale-105 transition-all duration-300 hover:drop-shadow-[0_0.25rem_.25rem_rgba(255,255,255,0.35)] group">
      { user && 
        <Link to="/account">
          <img src={`${avatarUrl}`} className="h-auto w-auto rounded-full"/>
          <div className="invisible absolute translate-y-2 -translate-x-1 text-lg opacity-0 group-hover:opacity-100 group-hover:visible transition-all p-1 rounded-md duration-500 mx-auto bg-dark text-light">Account</div>
          { user.admin &&
            <img src={admincrown} className="sticky -translate-y-[4.25rem] -rotate-[30deg] w-12 -translate-x-[0.4rem] group-hover:-rotate-45 group-hover:-translate-x-4 group-hover:-translate-y-[4.5rem] transition-all duration-300"/>
          }
        </Link>
      }
      { isLoading &&
        <>
          <img src={`${process.env.REACT_APP_IMG_SRC}/Discord-Logo-Color.svg`} className="h-auto w-auto grayscale"/>
          <img src={spinner} className="sticky h-auto w-auto -translate-y-[3rem] translate-x-2"/>
        </>
      }
      { error && 
        <>
          <img src={`${process.env.REACT_APP_IMG_SRC}/Discord-Logo-Color.svg`} className="h-auto w-auto bg-red-500/50 bg-blend-overlay rounded"/>
          <div className="invisible absolute translate-y-2 text-lg opacity-0 group-hover:opacity-100 group-hover:visible transition-all p-1 rounded-md duration-500 mx-auto bg-dark text-red-500">Error</div>
        </>
      }
      { !user && !isLoading && !error &&
        <button onClick={() => login && login()}>
          <img src={`${process.env.REACT_APP_IMG_SRC}/Discord-Logo-Color.svg`} className="h-auto w-auto"/>
          <div className="invisible absolute translate-y-2 text-lg opacity-0 group-hover:opacity-100 group-hover:visible transition-all p-1 rounded-md duration-500 mx-auto bg-dark text-light">Log in</div>
        </button>
      }
    </div>
  )
}

export default UserAvatar