import { FC } from 'react';
import PetGame from '../PetGame/PetGame';
import bg from '../assets/HomeBG.svg';
import useRandomQuote from '../hooks/useRandomQuote';

const Hero:FC = () => {
  const quote = useRandomQuote();

  return (
    <div className="snap-start snap-always flex flex-col items-center justify-evenly w-full min-h-screen bg-cover bg-bottom bg-no-repeat " style={{backgroundImage: `url(${bg})`}}>
      <div>
        { /*
        <h1 className="mx-auto text-center text-7xl sm:text-[8rem] pb-4 sm:pb-8 md:pt-8 mt-32 drop-shadow-[0_1rem_1rem_rgba(255,255,255,0.35)] bg-gradient-to-r from-primary to-tertiary bg-clip-text text-transparent font-semibold">
          Gooey.Dog
        </h1>
        */ }
        <img src={`${process.env.REACT_APP_IMG_SRC}/Logo-white.svg`} className="text-center h-60 md:h-72 mx-auto pb-4 sm:pt-4 md:pb-8 mt-32 drop-shadow-[0_0.5rem_0.5rem_rgba(255,255,255,0.15)]" />
        <p className="py-10 text-3xl text-neutral-300 text-center">
          {quote} 
        </p>
      </div>
      <PetGame />
    </div>
  );
};

export default Hero;
