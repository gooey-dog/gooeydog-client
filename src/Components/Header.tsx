import { FC, RefObject } from "react";
import { Link } from "react-router-dom";
import useScrollDirection from "../hooks/useScrollDirection";
import UserAvatar from "./UserAvatar";

const Header:FC<{threshold: number, fixed:boolean, containerRef?:RefObject<HTMLElement>}> = ({threshold, fixed, containerRef}) => {
  
  const scrollDirection = useScrollDirection(containerRef);

  return (
    <div className="w-full">
      <nav className={(fixed ? "fixed" : "") + " top-0 z-10 bg-gradient-to-b via-black from-black text-light flex flex-row justify-center w-full h-fit text-3xl sm:text-4xl transition-all duration-300 pb-14 " + (!scrollDirection ? "opacity-100 visible " : "invisible opacity-0")}>
        <Link to="/" className="py-4 px-2 sm:w-20 sm:mx-4 hidden sm:block hover:scale-105  hover:drop-shadow-[0_0.25rem_.25rem_rgba(255,255,255,0.35)] transition-all">
          <img src={`${process.env.REACT_APP_IMG_SRC}/Logo-Minimal.svg`} alt="" className="h-full"/>
        </Link>
        <div className="flex flex-row grow justify-center">
        <Link to="/art" className="py-8 px-2 hover:text-tertiary transition-all duration-500 hover:scale-105">Art</Link>
        <Link to="/commissions" className="py-8 px-2 hover:text-tertiary transition-all duration-500 hover:scale-105">Commissions</Link>
        <Link to="/other" className="py-8 px-2 hover:text-tertiary transition-all duration-500 hover:scale-105">Other</Link>
        </div>
        <UserAvatar />
      </nav>
    </div>
  );
};

export default Header;