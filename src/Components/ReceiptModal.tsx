import { useState, useEffect } from 'react';
import { Commission } from '../shared/types/CommissionInfoData'
import ContactLinks from './ContactLinks';

const ReceiptModal = ({
  commission,
  onClick
}: {
  commission: Commission;
  onClick: any;
}): JSX.Element => {
  const [fade, setFade] = useState<boolean>(false);
  const [fade2, setFade2] = useState<boolean>(false);
  useEffect(() => {
    setTimeout(() => setFade(true), 200);
    setTimeout(() => setFade2(true), 500);
  }, []);
  return <div className={"absolute text-dark text-xl flex top-0 z-50 bg-black/50 w-full h-screen transition-all duration-500 " + (fade ? "opacity-100" : "opacity-0")} onClick={onClick}>
      <div className={"bg-white w-fit max-w-lg m-auto p-16 rounded-xl transition-all duration-500 shadow-md shadow-white/30 " + (fade2 ? "opacity-100" : "opacity-0")}>
        <h1 className="text-transparent bg-gradient-to-br from-primary to-tertiary bg-clip-text text-6xl drop-shadow-md mb-4">
          Contact Me!
        </h1>
        <div className="inline">
          That comes to... 
        </div>
        <div className="inline px-2 underline decoration-2 decoration-primary">
          USD {commission.price?.toFixed(2)}
        </div>
        <div className="inline">
          !!
        </div>
        <div className="font-light">
          I appreciate your interest in my art! It means the world to me. I'd love to get in contact with you to discuss it further.
        </div>
        <div className="font-thin mt-4">
          Send me a screenshot of the following receipt to any of my contact links!
        </div>
        <table className="w-full my-4 border table-fixed text-sm align-top">
          <tbody className="">
            <tr>
              <td>Type</td>
              <td>{commission.type?.id} : {commission.type?.name}</td>
            </tr>
            <tr>
              <td>Style</td>
              <td>{commission.style?.id} : {commission.style?.name}</td>
            </tr>
            <tr>
              <td>Extras</td>
              {commission.extras?.map(extra => <td>{extra.id} : {extra.name}</td>)}
            </tr>
            <tr>
              <td>Background</td>
              <td>{commission.background?.id} : {commission.background?.name}</td>
            </tr>
            <tr>
              <td>Addons</td>
              {commission.addons?.map(addon => <td>{addon.id} : {addon.name}</td>)}
            </tr>
            <tr>
              <td>Subtotal</td>
              <td>{commission.price?.toFixed(2)}</td>
            </tr>
          </tbody>
        </table>
        <ContactLinks />
        <div className="text-dark text-lg font-extralight p-2">
          Tap anywhere to dismiss this prompt...
        </div>
      </div>
    </div>;
};
  
export default ReceiptModal;