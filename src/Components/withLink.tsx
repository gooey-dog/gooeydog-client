import React, { FC, } from "react";
import { Link } from "react-router-dom";

export default function withLink<T>(WrappedComponent: FC<T>): FC<T & {linkTo:string}> {

  const WithLink: FC<T & {linkTo: string}> = (props) => {
    return (
      <Link to={props.linkTo} className="transition-all hover:scale-105 hover:drop-shadow-[0_0.25rem_.25rem_rgba(255,255,255,0.35)]">
        <WrappedComponent {...props} />
      </Link>
    );
  }

  return WithLink;
}