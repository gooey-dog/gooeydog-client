import { FC, useEffect, useState } from 'react'

const WelcomeModal:FC = () => {
  const [hidden, setHidden] = useState<boolean>(false);
  const [welcome, setWelcome] = useState<boolean>(false);
  const [startFadeout, setStartFadeout] = useState<boolean>(false);

  useEffect( () => {
    setTimeout(() => {
      setWelcome(true);
    }, 200);
    setTimeout(() => {
      setStartFadeout(true);
    }, 1500);
    setTimeout(() => {
      setHidden(true);
    }, 2500);
  }, []);

  return (
    <div className={"flex justify-center items-center absolute w-screen h-screen bg-black text-light text-8xl z-50 transition-all duration-1000 " + (startFadeout ? "opacity-0 " : "opacity-100 ") + (hidden ? "hidden": "false")}>
      <div className={"transition-all duration-1000 " + (welcome ? "opacity-100" : "opacity-0")} >
        Welcome.
      </div>
    </div>
  )
}

export default WelcomeModal