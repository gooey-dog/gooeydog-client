import React, { FC } from 'react'

const IconInfoBox:FC<{title: string, body:Array<string>}> = ({title, body}) => {
  return (
    <div className="flex flex-row bg-white rounded-3xl w-auto mx-8 h-fit align-middle shadow-lg shadow-secondary/30 my-8 relative">
      <div className="absolute bg-gradient-to-br from-primary to-secondary via-amber-300 -translate-y-8 -translate-x-8 w-16 h-16 sm:w-20 sm:h-20 rounded-full shadow-lg shadow-black/25">
      </div>
      <div className="grow p-12 pl-8 text-xl sm:text-2xl md:text-3xl text-dark w-full font-thin">
        <h1 className="sm:text-3xl lg:text-4xl text-xl font-semibold pb-4">{title}</h1>
        {body.map((paragraph, index) => 
          <p key={index} className="py-1 px-4">{paragraph}</p>
        )}
      </div>
      <div className="w-8 bg-gradient-to-bl from-tertiary to-secondary overflow-clip bg-clip-border rounded-r-3xl">
      </div>
    </div>
  );
};

export default IconInfoBox;